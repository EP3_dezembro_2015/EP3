class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :category
      t.string :name
      t.integer :year
      t.string :description
      t.string :director

      t.timestamps null: false
    end
  end
end
