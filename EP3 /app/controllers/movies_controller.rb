class MoviesController < ApplicationController
	protect_from_forgery
	before_action :authenticate_user!, only:[:new,:create,:update,:edit,:destroy]
#The methode above works fine, remember to implement for method vote and for the comments for movies



	def index

				if  (params[:search]!=nil || ( !params[:search])) || (( !params[:search]!=nil) || params[:search])
								@movies = Movie.where("name like '%#{params[:search]}%'")					
							
					else
						@movies = Movie.all 
					end

	end
	

	def new

		@movie = Movie.new
	end
	
	
	def create
		@movie = Movie.new(movie_params)
		
		if @movie.save
			redirect_to @movie
		else
			render 'new'
		end
	end


	def show

		@movie = Movie.find(params[:id])	

	end

	
	def edit
	
		@movie= Movie.find(params[:id])
	
	end

	def category
		@movies = Movie.where(category: params[:category])
	end

	def update

		@movie= Movie.find(params[:id])
	
		if @movie.update_attributes(movie_params)
			redirect_to @movie
		else
			render 'edit'
		end
	
	end

	def destroy

		@movie= Movie.find(params[:id])
	    @movie.destroy

	    redirect_to 'home'

	end
	def movie_params

		params.require(:movie).permit(:name,:description,:director,:year,:category,:image,:nota,:voto,:media)		
	end


end
